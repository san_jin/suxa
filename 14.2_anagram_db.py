import shelve
from anagram_sets import *
def read_anagrams(db,word):
    '''
    look up a word and return a list of its anagrams
    :return:
    '''
    shf=shelve.open(db)
    word=signature(word)
    try:
        return shf[word]
    except KeyError:
        return []
def store_anagrams(filename,hist):
    '''
    store the anagram dictionary in a 'shelf'
    :return:
    '''
    db=shelve.open(filename,'c')
    for key,value in hist.items():

        db[key]=value
        print(key,value)
    db.close()
def main(filename='words.txt',db='shelf-1',word='post'):
    temp=all_anagrams(filename)
    # print(temp)
    store_anagrams(db,temp)
    res=read_anagrams(db,word)
    print(res)
if __name__ == '__main__':
    main()