from collections import defaultdict
temp=defaultdict(int)
def binomial_coeff(n,k):
    print(n,k)
    if temp[(n,k)]!=0:
        return temp[(n,k)]
    if k==0:
        temp[(n,k)]=1
        return 1
    if n==0:
        temp[(n,k)]=0
        return 0


    res=binomial_coeff(n-1,k)+binomial_coeff(n-1,k-1)
    print('res:',res)
    temp[(n,k)]=res
    print(temp)
    return res
print(binomial_coeff(4,2))